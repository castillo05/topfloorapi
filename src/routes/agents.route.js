'use strict'
import express from 'express';
import {authorization} from '../middleware/authenticate';

const api = express.Router();

import {getAgents} from '../controllers/Agents.controller';


api.get('/agents',authorization,getAgents);

module.exports = api;